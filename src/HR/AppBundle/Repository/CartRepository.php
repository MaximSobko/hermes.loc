<?php

namespace HR\AppBundle\Repository;

class CartRepository extends \Doctrine\ORM\EntityRepository
{
    public function countProducts($user_id, $user_token){
        $query = $this->createQueryBuilder('c');
        $query->select('SUM(c.quantity)');
        if($user_id){
           $query->andWhere('c.user = :user_id');
           $query->setParameter('user_id', $user_id);
        }
        elseif($user_token){
           $query->andWhere('c.userToken = :user_token');
           $query->andWhere('c.user is NULL');
           $query->setParameter('user_token', $user_token);
        }
        $products = $query
            ->getQuery()
            ->getSingleScalarResult();

        return $products ? $products : 0;
    }

    public function getProducts($user_id, $user_token){
        $query = $this->createQueryBuilder('c');
        if($user_id){
           $query->andWhere('c.user = :user_id');
           $query->setParameter('user_id', $user_id);
        }
        elseif($user_token){
           $query->andWhere('c.userToken = :user_token');
           $query->andWhere('c.user is NULL');
           $query->setParameter('user_token', $user_token);
        }
        $products = $query
            ->getQuery()
            ->getResult();

        return $products;
    }

    public function getProduct($product_id, $user_id, $user_token){
        $cart = null;
        if($user_id || $user_token){
            $query = $this->createQueryBuilder('c');
            $query->where('c.product = :product_id');
            $query->setParameter('product_id', $product_id);
            if($user_id){
                $query->andWhere('c.user = :user_id');
                $query->setParameter('user_id', $user_id);
            }
            elseif($user_token){
                $query->andWhere('c.userToken = :user_token');
                $query->andWhere('c.user is NULL');
                $query->setParameter('user_token', $user_token);
            }
            $query->setMaxResults(1);
            $cart = $query
            ->getQuery()
            ->getOneOrNullResult();
        }
        return $cart;

    }
    public function getCurrentCart($id, $user_id, $user_token){
        $cart = null;
        if($user_id || $user_token){
            $query = $this->createQueryBuilder('c');
            $query->where('c.id = :id');
            $query->setParameter('id', $id);
            if($user_id){
                $query->andWhere('c.user = :user_id');
                $query->setParameter('user_id', $user_id);
            }
            elseif($user_token){
                $query->andWhere('c.userToken = :user_token');
                $query->andWhere('c.user is NULL');
                $query->setParameter('user_token', $user_token);
            }
            $query->setMaxResults(1);
            $cart = $query
            ->getQuery()
            ->getOneOrNullResult();
        }
        return $cart;

    }
    public function deleteCartByUser($user_id){
        $query = $this->createQueryBuilder('c');
        $query->delete();
        $query->where('c.user = :user_id');
        $query->setParameter('user_id', $user_id);
        return $query->getQuery()
                  ->getResult();
    }
    public function updateCart($user_id, $user_token){
        $query = $this->createQueryBuilder('c');
        $query->update()->set('c.user', $user_id);
        $query->andWhere('c.userToken = :user_token');
        $query->andWhere('c.user is NULL');
        $query->setParameter('user_token', $user_token);
        return $query->getQuery()
                  ->getResult();
    }

}
