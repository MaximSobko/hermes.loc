<?php

namespace HR\AppBundle\Repository;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FavouriteRepository extends \Doctrine\ORM\EntityRepository
{
	public function checkProduct($product_id, $user_id, $user_token){
        $query = $this->createQueryBuilder('f');

        $query->where('f.product = :product_id');
        $query->setParameter('product_id', $product_id);

        if($user_id){
           $query->andWhere('f.user = :user_id');
           $query->setParameter('user_id', $user_id);
        }
        elseif($user_token){
           $query->andWhere('f.userToken = :user_token');
           $query->andWhere('f.user is NULL');
           $query->setParameter('user_token', $user_token);
        }

        $products = $query
            ->getQuery()
            ->getOneOrNullResult();
        return $products;
    }

   	public function checkProducts($product_ids, $user_id, $user_token){
        $query = $this->createQueryBuilder('f');
        $query->select('p.id');
        $query->leftJoin('f.product', 'p');
        $query->where('f.product IN (:product_ids)');
        $query->setParameter('product_ids', $product_ids);

        if($user_id){
           $query->andWhere('f.user = :user_id');
           $query->setParameter('user_id', $user_id);
        }
        elseif($user_token){
           $query->andWhere('f.userToken = :user_token');
           $query->andWhere('f.user is NULL');
           $query->setParameter('user_token', $user_token);
        }

        $products = $query
            ->getQuery()
            ->getArrayResult();
        if($products) $products = array_column($products, 'id');
        return $products;
    } 
    public function updateFavourites($user_id, $user_token){
        $query = $this->createQueryBuilder('f');
        $query->update()->set('f.user', $user_id);
        $query->andWhere('f.userToken = :user_token');
        $query->andWhere('f.user is NULL');
        $query->setParameter('user_token', $user_token);
        return $query->getQuery()
                  ->getResult();
    }

    public function getAllByNotLogged($user_token){
    	$query = $this->createQueryBuilder('f');
        $query->select('p.id');
        $query->leftJoin('f.product', 'p');
        $query->andWhere('f.userToken = :user_token');
        $query->andWhere('f.user is NULL');
        $query->setParameter('user_token', $user_token);

        $products = $query
            ->getQuery()
            ->getArrayResult();
        if($products) $products = array_column($products, 'id');
        return $products;
    }
    public function deleteByUserAndProductIds($user_id, $productIds){
    	$query = $this->createQueryBuilder('f');
        $query->delete();
        $query->where('f.user = :user_id');
        $query->setParameter('user_id', $user_id);

        $query->andWhere('f.product IN (:product_ids)');
        $query->setParameter('product_ids', $productIds);
        return $query->getQuery()
                  ->getResult();
    }
}
