<?php

namespace HR\AppBundle\Repository;


class OrderProductRepository extends \Doctrine\ORM\EntityRepository
{
	public function multiInsert($inserts){
		$em = $this->getEntityManager();
        $connection = $em->getConnection();
        $table = 'order_products';
        if ($inserts && count($inserts) > 0) {
            $values = [];
            $columns = implode(',', array_keys($inserts[0]));
            foreach ($inserts as $key => $i) {
                $values[] = "('" . implode("','", $i) . "')";
            }
            $values = implode(',', $values);

            $sql = "INSERT INTO $table({$columns}) VALUES $values";
            $statement = $connection->prepare($sql);
            $statement->execute();
        }
	}
}
