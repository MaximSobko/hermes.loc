<?php

namespace HR\AppBundle\Listener;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\EventDispatcher\Event;

class ResponseListener
{
    public function setUserUniqueId(Event $event)
    {
        $response = $event->getResponse();
        $request = $event->getRequest();
        $cookies = $request->cookies;
        if(!$cookies->get('user-token')){
            $response->headers->setCookie(new Cookie("user-token", $this->getUserUniqueCookie(), time() + (10 * 365 * 24 * 60 * 60)));
        }
    }

    protected function getIp()
    {
        $ip = null;
        if (!empty($_SERVER['HTTP_X_REAL_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    protected function getUserUniqueCookie()
    {
        return sha1($this->getIp() . '-HERMES-' . time());
    }

}