<?php 
namespace HR\AppBundle\Listener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class LoginListener
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $request = $this->container->get('request');

        if ($event instanceof InteractiveLoginEvent) {
            $user = $event->getAuthenticationToken()->getUser();
            $userId = $user ? $user->getId() : null;
            $userToken = $request->cookies->get('user-token'); # get userToken
            $this->getDoctrine()->getRepository('HRAppBundle:Cart')->deleteCartByUser($userId, $userToken);
            $this->getDoctrine()->getRepository('HRAppBundle:Cart')->updateCart($userId, $userToken);

            $notLoggedFavourite = $this->getDoctrine()->getRepository('HRAppBundle:Favourite')->getAllByNotLogged($userToken);

            if($notLoggedFavourite){
                $this->getDoctrine()->getRepository('HRAppBundle:Favourite')->deleteByUserAndProductIds($userId, $notLoggedFavourite);
            }

            $this->getDoctrine()->getRepository('HRAppBundle:Favourite')->updateFavourites($userId, $userToken);
         }
    }
    public function getDoctrine(){
        return $this->container->get('doctrine');
    }
}