<?php

namespace HR\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use HR\AppBundle\Entity\Favourite;

class FavouriteController extends Controller
{
    public function modifyAction(Request $request, $id){
        $user = $this->getUser();
        $success = false;
        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken
        $em = $this->getDoctrine()->getManager();

        $product = $this->getDoctrine()->getRepository('HRAppBundle:Product')->find($id);
        if($product){
	        $favourite = $this->getDoctrine()->getRepository('HRAppBundle:Favourite')->checkProduct($id, $userId, $userToken);
	        if($favourite){
	            $em->remove($favourite);
	        }
	        else{
	        	$favourite = new Favourite();
	        	$favourite->setUserToken($userToken);
	        	$favourite->setUser($user);
	        	$favourite->setProduct($product);
	        	$em->persist($favourite);
	        }
	        $em->flush();
	        $success = true;
    	}
        return new JsonResponse([
        		'success' => $success
            ]);
    }
}
