<?php

namespace HR\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    public function indexAction(Request $request)
    {
        $search = $request->get('q');
        $products = [];
        if ($search) {
            $products = $this->getDoctrine()->getRepository('HRAppBundle:Product')->searchProducts($search);
        }
        return $this->render('HRAppBundle:Search:index.html.twig',
            [
                'products' => $products,
                'search' => $search
            ]
        );
    }
}
