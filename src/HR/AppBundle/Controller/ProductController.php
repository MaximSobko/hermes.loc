<?php

namespace HR\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use HR\AppBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Request; # подключенный request из namespace

class ProductController extends Controller
{
    public function indexAction(Request $request, $product_id) # передача Request из namespace в action
    {
        $user = $this->getUser();
        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken

        $product = $this->getDoctrine()->getRepository('HRAppBundle:Product')->find($product_id);

    	$breadcrumbs = $this->get('app.breadcrumbs');
        $breadcrumbs->setBreadcrumbs(
                ['url' => $this->generateUrl('homepage'),
                 'title' => 'Home'
                ],
                ['url' => $this->generateUrl('catalog', ['gender' => $product->getGender()]),
                'title' => $product->getGenderName()
                ],
                ['url' => '',
                 'title' => $product->getTitle()
                ]
            );
        /* Один из вариантов реализации
    	$breadcrumbs->add(
    		['url' => $this->generateUrl('homepage'),
    		 'title' => 'Home'
    		]);

         $breadcrumbs->add(
            ['url' => $this->generateUrl('catalog', ['gender' => $product->getGender()]),
             'title' => $product->getGenderName()
            ]);
         
        $breadcrumbs->add( 
            ['url' => '',
             'title' => $product->getTitle()
            ]);
            */


        $related = $this->getDoctrine()->getRepository('HRAppBundle:Product')->getRelated($product_id, 0, 0);

        $productIds = [];
        foreach($related as $p){
            $productIds[] = $p->getId();
        }
        $favourites = $this->getDoctrine()->getRepository('HRAppBundle:Favourite')->checkProducts($productIds,$userId, $userToken);

        return $this->render('HRAppBundle:Product:index.html.twig', [
            'product' => $product,
            'related' => $related,
            'favourites' => $favourites,
            'breadcrumbs' => $breadcrumbs
            ]);
    }
}