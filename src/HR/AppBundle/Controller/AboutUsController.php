<?php

namespace HR\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AboutUsController extends Controller
{
    public function indexAction()
    {
    	$breadcrumbs = $this->get('app.breadcrumbs');
        $breadcrumbs->setBreadcrumbs(
                ['url' => $this->generateUrl('homepage'),
                 'title' => 'Home'
                ],
                ['url' => '',
                'title' => 'About us'
                ]
            );
        return $this->render('HRAppBundle:AboutUs:index.html.twig',
        		[
        			'breadcrumbs' => $breadcrumbs
        		]
        	);
    }
}
