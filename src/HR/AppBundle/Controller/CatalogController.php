<?php

namespace HR\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use HR\AppBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Request; # подключенный request из namespace

class CatalogController extends Controller
{
    public function indexAction(Request $request) # передача Request из namespace в action
    {
        $user = $this->getUser();
        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken
        $page = $request->get('page'); # получаем текущую страницу из GET
        if (!$page) $page = 1;
        $limit = Product::LIMIT;
        $gender = $request->get('gender');
        $accessory = $request->get('accessories');
        $products = $this->getDoctrine()->getRepository('HRAppBundle:Product')->getProductByPage($page, $gender, $accessory);

        $productIds = [];
        foreach($products as $p){
            $productIds[] = $p->getId();
        }
        $favourites = $this->getDoctrine()->getRepository('HRAppBundle:Favourite')->checkProducts($productIds,$userId, $userToken);

        $countProduct = $this->getDoctrine()->getRepository('HRAppBundle:Product')->countProducts($gender, $accessory);
        $pages = ceil($countProduct / $limit);

        $breadcrumbs = $this->get('app.breadcrumbs');

        $title = "All";
        if($accessory){
            $title = "Accessory";
        }
        elseif($gender){
            $title =Product::$genderNames[$gender];
        } 

        $breadcrumbs->setBreadcrumbs(
                ['url' => $this->generateUrl('homepage'),
                 'title' => 'Home'
                ],
                ['url' => '',
                'title' => $title
                ]
            );
        return $this->render('HRAppBundle:Catalog:index.html.twig', [
            'page' => $page,
            'pages' => $pages,
            'limit' => $limit,
            'count' => $countProduct,
            'products' => $products,
            'favourites' => $favourites,
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
