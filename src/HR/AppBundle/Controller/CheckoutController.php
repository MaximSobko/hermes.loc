<?php

namespace HR\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use HR\AppBundle\Form\OrderType;
use HR\AppBundle\Entity\Order;

class CheckoutController extends Controller
{
    public function indexAction(Request $request){
        $user = $this->getUser();
        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken

        $products = $this->getDoctrine()->getRepository('HRAppBundle:Cart')->getProducts($userId, $userToken);
        if(!$products){
        	return $this->redirectToRoute('homepage');
        }
        return $this->render('HRAppBundle:Checkout:index.html.twig', [
        		'products' => $products
        	]);
    }
    public function readyAction(Request $request){

        $user = $this->getUser();

        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken

        $carts = $this->getDoctrine()->getRepository('HRAppBundle:Cart')->findBy(['user' => $userId]);

        $form = $this->createForm(OrderType::class, new Order());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {



            $order = $form->getData();
            $order->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            $inserts = [];
            foreach($carts as $c){
                $p = $c->getProduct();
                $inserts[] = [
                    'order_id' => $order->getId(),
                    'product_id' => $p->getId(),
                    'product_title' => $p->getTitle(),
                    'product_price' => $p->getDiscountPrice(),
                    'product_quantity' => $c->getQuantity(),
                ];
            }
            $this->getDoctrine()->getRepository('HRAppBundle:OrderProduct')->multiInsert($inserts);
            $this->getDoctrine()->getRepository('HRAppBundle:Cart')->deleteCartByUser($userId);

            # Setup the message
            $message = \Swift_Message::newInstance()
                ->setSubject('Order test')
                ->setFrom('0984326650maxim@gmail.com')
                ->setTo($order->getEmail())
                ->setBody($this->get('templating')->render('mail/order.html.twig', ['order' => $order]), 'text/html');

            # Setup the message
            $messageToManager = \Swift_Message::newInstance()
                ->setSubject('Order test')
                ->setFrom('0984326650maxim@gmail.com')
                ->setTo($this->getParameter('manager_email'))
                ->setBody($this->get('templating')->render('mail/order.html.twig', ['order' => $order]), 'text/html');

            
            $sent = $this->get('mailer')
                ->send($message);

            $this->get('mailer')->send($messageToManager);

            $this->addFlash(
                'thankyou',
                "Thank you for your order! Order number #".$order->getId()
            );

            return $this->redirectToRoute('checkout_thankyou');
            //echo $order->getEmail();


        }
        if(!$carts){
            return $this->redirectToRoute('homepage');
        }
        return $this->render('HRAppBundle:Checkout:ready.html.twig', [
                'products' => $carts,
                'form' => $form->createView()
            ]);
    }

    public function thankyouAction(Request $request){
            $session = $this->get('session');
            $checkMessage = $session->getFlashBag()->has('thankyou');
            if(!$checkMessage){
                return $this->redirectToRoute('homepage');
            }
            return $this->render('HRAppBundle:Checkout:thankyou.html.twig');
    }
}
