<?php

namespace HR\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken

        $slides = $this->getDoctrine()->getRepository('HRAppBundle:Slides')->findAll();
        $categories = $this->getDoctrine()->getRepository('HRAppBundle:Category')->findAll();

        $products = $this->getDoctrine()->getRepository('HRAppBundle:Product')->findBy([],[], 12);
        $productIds = [];
        foreach($products as $p){
            $productIds[] = $p->getId();
        }
        $favourites = $this->getDoctrine()->getRepository('HRAppBundle:Favourite')->checkProducts($productIds,$userId, $userToken);
        return $this->render('HRAppBundle:Home:index.html.twig',
            [
                'slides' => $slides,
                'categories' => $categories,
                'products' => $products,
                'favourites' => $favourites
            ]
        );
    }
}
