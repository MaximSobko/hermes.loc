<?php

namespace HR\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


class CartController extends Controller
{
    public function indexAction(Request $request){
        $user = $this->getUser();
        $count = 0;

        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken

        $count = $this->getDoctrine()->getRepository('HRAppBundle:Cart')->countProducts($userId, $userToken);
        return $this->render('::components/cart.html.twig', [
            'count' => $count,
        ]);
    }
    public function addAction(Request $request, $product_id)
    {   
        $success = false;
        $message = 'Product not added';
        $user = $this->getUser();
        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken

        $em = $this->getDoctrine()->getEntityManager();
        $product = $this->getDoctrine()->getRepository('HRAppBundle:Product')->find($product_id);
        if($product){
             $cart = $this->getDoctrine()->getRepository('HRAppBundle:Cart')->getProduct($product_id, $userId, $userToken);
             $quantity = intval($request->get('quantity')) > 0 ? intval($request->get('quantity')) : 1;
             if(!$cart){
                $cart = new \HR\AppBundle\Entity\Cart();
                $cart->setQuantity($quantity);
             }
             else{
                $cart->setQuantity($cart->getQuantity() + $quantity);
             }
             $cart->setProduct($product);
             if($user) $cart->setUser($user);
             if($userToken) $cart->setUserToken($userToken);
             $em->persist($cart);
             $em->flush();
             $message = "{$product->getTitle()} - added. Quantity: {$cart->getQuantity()}";
             $success = true;
        }
        return new JsonResponse(
            [
                'success' => $success,
                'message' => $message
            ]
        );
    }
    public function editAction(Request $request, $id){
        $em = $this->getDoctrine()->getEntityManager();
        $success = false;
        $message = 'Cart not found';
        $count = 0;
        $user = $this->getUser();
        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken

        $quantity = intval($request->get('quantity')) > 0 ? intval($request->get('quantity')) : 1;
       $cart = $this->getDoctrine()->getRepository('HRAppBundle:Cart')->getCurrentCart($id, $userId, $userToken);

        if($cart){
                $success = true;
                $cart->setQuantity($quantity);
                $em->persist($cart);
                $em->flush();
                $message = "{$cart->getProduct()->getTitle()} - changed. Quantity: {$cart->getQuantity()}";
                $count = $this->getDoctrine()->getRepository('HRAppBundle:Cart')->countProducts($userId, $userToken);
        }
        return new JsonResponse(
            [
                'success' => $success,
                'message' => $message,
                'count' => $count
            ]
        );
    }

    public function deleteAction(Request $request, $id){
        $em = $this->getDoctrine()->getEntityManager();
        $success = false;
        $message = 'Cart was deleted';
        $count = 0;
        $user = $this->getUser();
        $userId = $user ? $user->getId() : null;
        $userToken = $request->cookies->get('user-token'); # get userToken

       $cart = $this->getDoctrine()->getRepository('HRAppBundle:Cart')->getCurrentCart($id, $userId, $userToken);

        if($cart){
                $success = true;
                $message = "{$cart->getProduct()->getTitle()} - removed.";
                $em->remove($cart);
                $em->flush();
          
                $count = $this->getDoctrine()->getRepository('HRAppBundle:Cart')->countProducts($userId, $userToken);
        }
        return new JsonResponse(
            [
                'success' => $success,
                'message' => $message,
                'count' => $count
            ]
        );
    }
}
