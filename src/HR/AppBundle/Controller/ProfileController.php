<?php

namespace HR\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use HR\AppBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Request; # подключенный request из namespace
use HR\AppBundle\Form\UserType;
use HR\AppBundle\Entity\User;

class ProfileController extends Controller
{
    public function indexAction(Request $request) 
    {
    	$user = $this->getUser();

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        	$user = $form->getData();

        	$encoder = $this->container->get('security.password_encoder');
			$encoded = $encoder->encodePassword($user, $user->getPassword());

			$user->setPassword($encoded);
   			
        	$em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
        }
    	$lastOrder = $this->getDoctrine()->getRepository('HRAppBundle:Order')->findOneBy(['user' => $user->getId()], ['id' => 'DESC']);

        $favourites = $this->getDoctrine()->getRepository('HRAppBundle:Favourite')->findBy(['user' => $user->getId()], ['createdAt' => 'DESC'], 10);


        return $this->render('HRAppBundle:Profile:index.html.twig', [
        		'last_order' => $lastOrder,
        		'form' => $form->createView(),
                'favourites' => $favourites
        	]);
    }
}