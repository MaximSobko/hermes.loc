<?php

namespace HR\AppBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Breadcrumbs
{
	private $container;
	private $breadcrumbs = [];
	private $template = 'components/breadcrumbs.html.twig';

	public function __construct(ContainerInterface $c){
		$this->container = $c;
	}

	public function setTemplate($template){
		$this->template = $template;
		return $this;
	}

	public function add($breadcrumb){
		$this->breadcrumbs[] = $breadcrumb;
		return $this;
	}

	public function setBreadcrumbs(){
		$this->breadcrumbs = func_get_args();
		return $this;
	}

    public function render(){
    	return $this->container->get('templating')->render($this->template, ['data' => $this->breadcrumbs]);
    }
}