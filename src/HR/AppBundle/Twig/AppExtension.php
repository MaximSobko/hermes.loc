<?php

namespace HR\AppBundle\Twig;

class AppExtension extends \Twig_Extension
{   

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('app_unset', array($this, 'unsetFilter')),
        );
    }

    public function getFunctions(){
        return array(
            'price' => new \Twig_Function_Method($this, 'priceFunction')
        );
    }

    public function unsetFilter($params, $deleted)
    {
        unset($params[$deleted]);
        return $params;
    }

    public function priceFunction(){
        return 1;
    }




    public function getName()
    {
        return 'app_extension';
    }
}