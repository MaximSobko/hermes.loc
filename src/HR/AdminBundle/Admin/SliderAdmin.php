<?php

namespace HR\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SliderAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add ('subtitle');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('title', null, array('editable' => true))
            ->add('subtitle')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getSubject();
        $formMapper
            ->add('image', 'file', $this->getImageOptions($object))
            ->add('title')
            ->add('subtitle');
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('subtitle');
    }

    public function prePersist($object)
    {
        $em = $this->getDoctrine()->getManager(); # менеджер сущностей
        // сохранение сущности обьекта для получения Id и в дальнейшем сохрании картинки в папку
        $em->persist($object);
        $em->flush();
        $this->saveFile($object);
    }

    public function preUpdate($object)
    {
        $this->saveFile($object);
    }

    public function saveFile($object)
    {
        $request = $this->request;
        $basepath = $this->getConfigurationPool()->getContainer()->getParameter('upload_dir') . '/..' . $object->getTemplateDir();


        $oldImage = $basepath.'/'.$request->get('image'); # генерю старый путь к файлу существует он или нет это не важно я это проверю в методе deleteOldImage
        $image = $object->getImage(); # получение изображения загруженного
        $ext_arr = ['gif', 'jpg', 'jpeg', 'png', 'bmp']; # массив допустимых разширений

        if (!$request->get('delete_image')) {
            if (is_object($image)) {
                $info = pathinfo($image->getClientOriginalName()); #берем информацию о файле
                if (in_array(strtolower($info['extension']), $ext_arr)) { # проверяем разширения
                    if ($image->move($basepath, $image->getClientOriginalName())) { # перемещаем файл в папку
                        $this->deleteOldImage($oldImage); # удаляем старый файл
                        $file = substr(md5($image . time()), 0, 20); # задаем случайное имя
                        $file .= '.' . $info['extension'];
                        rename($basepath . '/' . $info['basename'], $basepath . '/' . $file); #переименновываем
                        $object->setImage($file); #записываем в сущность
                    }
                }
            } else {
                $object->setImage($request->get('image'));
            }
        } else {
            $this->deleteOldImage($oldImage); # удаляем старый файл если у нас клацнули на delete
        }
    }

    public function deleteOldImage($path){ // удаление старого файла is_file - являеться это файлом(не папкой) и file_exist проверка на существование
        file_exists($path) && is_file($path) ? unlink($path) : false;
    }

    public function getDoctrine()
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine');
    }

    public function getImageOptions($object, $field = 'image')
    {
        $options = array('required' => false);
        $fieldLower = $field; # оставляем image
        $field = ucfirst($field); # преобразовываем первую букву Image

        $webPathMethod = 'get' . $field; # генерим строку метода
        $fullPathMethod = $webPathMethod . 'Url'; # относительный путь файла

        $fullPath = $object->$fullPathMethod(); # вызов метода

        $webPath = $object->$webPathMethod();  # вызов метода

        if ($object && $webPath) {
            $options['help'] = '<img src="' . $fullPath . '" class="admin-preview" /><input  type="hidden" name="' . $fieldLower . '" value="' . $webPath . '"/><br><div style="line-height:20px;"><input type="checkbox" name="delete_' . $fieldLower . '" id="delete_' . $fieldLower . '"/> <label for="delete_' . $fieldLower . '" style="margin-left:7px;padding-top:5px;display:inline-block;"> Delete Image </label></div>';
            $options['data_class'] = null;
            $options['label'] = $field;
        }
        return $options;
    }
}
