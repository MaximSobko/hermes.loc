<?php

namespace HR\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('HRAdminBundle:Default:index.html.twig');
    }
}
