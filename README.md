
##Virtual Hosts

```
<VirtualHost *:80>
 DocumentRoot "C:/wamp64/www/hermes/web"
 ServerName hermes
 ServerAlias hermes
 <Directory "C:/wamp64/www/hermes/web">
  AllowOverride None
  RewriteEngine On
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)$ app_dev.php [QSA,L]
 </Directory>
</VirtualHost>
```

###Быстрый старт

1. Сверху навести на ...(в панеле слева)
2. Найти кнопку "Clone"
3. Кликнуть по ней
4. Выбрать в выпадалке вместо SSH -> HTTPS
5. Скопировать
6. Открыть git bash и перейти в проекты
7. Вставить то что скопировали и клацнуть enter
8. Дать права на кэш и логи и создать базу(chmod 777 -R app/cache, chmod 777 -R app/logs)
9. Запустить composer update(ввести данные которые нужно для базы)
